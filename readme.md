0. download repository
======
1. create environments
======
##for Windows

##using virtualenv (must be in project folder)

* virtualenv env
* cd env
* cd scripts
* activate
* cd ..
* cd ..
* pip install -r requirements.txt

---

##using conda

* conda env create -f environment.yml
---
2. create .env file in folder
======

provide .env file wih attributes SECRET_KEY (string) and DEBUG=True
---
3. make migrations, migrate
======
* python manage.py makemigrations
* python manage.py migrate
* python manage.py runserver
---
4. to reach admin site create superuser
======
* python manage.py createsuperuser
