from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import CustomUser
from .models import Profile

from timebyter.models import Project, Task, TaskPart

class ProjectInline(admin.StackedInline):
    model = Project


# Define a new User admin
class CustomUserAdmin(UserAdmin):
    inlines = (ProjectInline,)

admin.site.register(CustomUser, CustomUserAdmin)
admin.site.register(Profile)
