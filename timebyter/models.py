from django.db import models
from django.conf import settings

import datetime

class Project(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE
    )
    title = models.CharField(max_length=50)
    description = models.TextField(null=True, blank=True)
    started_on = models.DateTimeField(default=datetime.datetime.now)
    finished_on = models.DateTimeField(blank=True, null=True)

    class Meta:
        unique_together = (("user", "title"),)

    @property
    def start(self):
        return self.started_on.strftime('%c')

    @property
    def finish(self):
        return self.finished_on.strftime('%c') if self.finished_on else None

    @property
    def finished(self):
        if self.finish:
            return True
        else:
            return False

    @property
    def number_of_tasks(self):
        return self.tasks.count()

    @property
    def get_project_duration(self):
        duration = datetime.timedelta(0)
        if self.tasks.all():
            tasks = self.tasks.all()

            for task in tasks:
                duration += task.get_duration

        return duration

    @property
    def project_duration(self):
        return str(self.get_project_duration)

    def __str__(self):
        return self.title


class Task(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE, related_name='tasks')
    title = models.CharField(max_length=50)
    description = models.TextField(null=True, blank=True)



    @property
    def get_duration(self):
        duration = datetime.timedelta(0)
        if self.taskparts.all():
            taskparts = self.taskparts.all()

            for taskpart in taskparts:
                duration += taskpart.get_duration

        return duration

    @property
    def duration(self):
        return str(self.get_duration)

    def __str__(self):
        return self.title


class TaskPart(models.Model):
    task = models.ForeignKey(Task, on_delete=models.CASCADE, related_name='taskparts')
    start = models.DateTimeField(default=datetime.datetime.now)
    stop = models.DateTimeField(blank=True, null=True)

    @property
    def get_duration(self):
        taskpartduration = datetime.timedelta(0)
        if self.stop:
            return self.stop-self.start
        else:
            return taskpartduration

    @property
    def duration(self):
        return str(self.get_duration)

    def __str__(self):
        return str(self.get_duration)
