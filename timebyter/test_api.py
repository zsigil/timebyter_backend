#from django.test import TestCase

from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from timebyter.models import Project, Task, TaskPart
from accounts.models import CustomUser

import datetime
import time #to wait for stopwatches

class ProjectCreateTests(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = CustomUser.objects.create(username="olivia", email="olivia@test.com", password="secret123")
        self.token = Token.objects.create(user=self.user)

    def test_create_project_not_authenticated(self):
        """
        Ensure that unauthenticated users cannot create a new project object.
        """
        url = '/api/projects/'
        # url = reverse('project-list')
        data = {'title': 'Testproject'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(Project.objects.count(), 0)

    def test_create_project_authenticated(self):
        """
        Ensure we can create a new project object.
        """
        # self.client.force_login(user=self.user)

        url = '/api/projects/'
        data = {'title': 'Testproject'}
        tokenauth = 'Token {}'.format(self.token)
        response = self.client.post(url, data, format='json', HTTP_AUTHORIZATION=tokenauth )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Project.objects.count(), 1)
        self.assertEqual(Project.objects.get().title, 'Testproject')
        self.assertEqual(response.data['title'], 'Testproject')
        self.assertEqual(response.data['id'], 1)
        self.assertEqual(response.data['user'], 1)
        self.assertEqual(response.data['finish'], None)
        self.assertEqual(response.data['description'], None)
        self.assertTrue(datetime.datetime.strptime(response.data['start'],'%c')<datetime.datetime.now()+datetime.timedelta(seconds=1))
        self.assertTrue(datetime.datetime.strptime(response.data['start'],'%c')>datetime.datetime.now()-datetime.timedelta(seconds=1))

    def test_create_project_with_too_long_title(self):
        """
        Title with more than 50 characters is not accepted.
        """
        # self.client.force_login(user=self.user)

        url = '/api/projects/'
        data = {'title': '012345678901234567890123456789012345678901234567891'}
        tokenauth = 'Token {}'.format(self.token)
        response = self.client.post(url, data, format='json', HTTP_AUTHORIZATION=tokenauth )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_project_without_title_field(self):
        """
        Ensure we can create a new project object.
        """
        # self.client.force_login(user=self.user)

        url = '/api/projects/'
        data = {'description': 'description'}
        tokenauth = 'Token {}'.format(self.token)
        response = self.client.post(url, data, format='json', HTTP_AUTHORIZATION=tokenauth )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class ProjectListRetrieveUpdateDeleteTests(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.user1 = CustomUser.objects.create(username="olivia", email="olivia@test.com", password="secret123")
        self.token1 = Token.objects.create(user=self.user1)

        self.user2 = CustomUser.objects.create(username="olivia2", email="olivia2@test.com", password="secret123")
        self.token2 = Token.objects.create(user=self.user2)

        project1 = Project.objects.create(user=self.user1, title='user1project')
        project2 = Project.objects.create(user=self.user2, title='user2project')
        project2b = Project.objects.create(user=self.user2, title='user2projectb')

    def test_list_unauthenticated(self):
        """
        Ensure only authenticated user can list.
        """

        url = '/api/projects/'
        # url = reverse('project-list')
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(Project.objects.count(), 3)

    def test_list_own_projects1(self):
        """
        A)Ensure user can only list own project object.
        """
        # self.client.force_login(user=self.user1)
        tokenauth = 'Token {}'.format(self.token1)

        url = '/api/projects/'
        # url = reverse('project-list')
        response = self.client.get(url, HTTP_AUTHORIZATION=tokenauth)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Project.objects.count(), 3)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]['title'], 'user1project')
        self.assertEqual(response.data[0]['user'], 1)

    def test_list_own_projects2(self):
        """
        B)Ensure user can only list own project object.
        """
        # self.client.force_login(user=self.user2)
        tokenauth = 'Token {}'.format(self.token2)

        url = '/api/projects/'
        # url = reverse('project-list')
        response = self.client.get(url, HTTP_AUTHORIZATION=tokenauth)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Project.objects.count(), 3)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(response.data[0]['user'], 2)
        self.assertEqual(response.data[1]['user'], 2)

    def test_retrieve_project_authenticated_authorized(self):
        """
        Ensure user can get own project object.
        """
        # self.client.force_login(user=self.user1)
        tokenauth = 'Token {}'.format(self.token1)

        url = '/api/projects/1/'
        response = self.client.get(url, HTTP_AUTHORIZATION=tokenauth)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['user'], 1)
        self.assertEqual(response.data['id'], 1)

    def test_retrieve_project_unauthorized(self):
        """
        Ensure user can not get others' project object.
        """
        # self.client.force_login(user=self.user1)
        tokenauth = 'Token {}'.format(self.token1)

        url = '/api/projects/2/'
        response = self.client.get(url, HTTP_AUTHORIZATION=tokenauth)

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


    def test_update_project_authenticated_authorized(self):
        """
        Ensure user can update own project
        """

        # self.client.force_login(user=self.user1)
        tokenauth = 'Token {}'.format(self.token1)

        data = {'title': 'newtitle'}
        url = '/api/projects/1/'
        response = self.client.patch(url, data, HTTP_AUTHORIZATION=tokenauth)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['user'], 1)
        self.assertEqual(response.data['title'], 'newtitle')

    def test_update_project_unauthorized(self):
        """
        Ensure user cannot update other's project
        """

        # self.client.force_login(user=self.user1)
        tokenauth = 'Token {}'.format(self.token1)

        data = {'title': 'newtitle'}
        url = '/api/projects/2/'
        response = self.client.patch(url, data, HTTP_AUTHORIZATION=tokenauth)

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_update_project_authenticated_authorized_id_user_not_changed(self):
        """
        Ensure user can not update project's id or user.
        """

        # self.client.force_login(user=self.user1)
        tokenauth = 'Token {}'.format(self.token1)

        data = {'id': 3, 'user': 2}
        url = '/api/projects/1/'
        response = self.client.patch(url, data, HTTP_AUTHORIZATION=tokenauth)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['user'], 1)
        self.assertEqual(response.data['id'], 1)

    def test_delete_project_unauthorized(self):
        """
        Ensure user can not delete project of others.
        """
        # self.client.force_login(user=self.user1)
        tokenauth = 'Token {}'.format(self.token1)

        url = '/api/projects/2/'
        response = self.client.delete(url, HTTP_AUTHORIZATION=tokenauth)

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_delete_project_authorized(self):
        """
        Ensure user delete own project.
        """
        # self.client.force_login(user=self.user1)
        tokenauth = 'Token {}'.format(self.token1)

        url = '/api/projects/1/'
        response = self.client.delete(url, HTTP_AUTHORIZATION=tokenauth)

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


class ProjectActionTests(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.user1 = CustomUser.objects.create(username="olivia", email="olivia@test.com", password="secret123")
        self.token1 = Token.objects.create(user=self.user1)

        self.user2 = CustomUser.objects.create(username="olivia2", email="olivia2@test.com", password="secret123")
        self.token2 = Token.objects.create(user=self.user2)

        project1 = Project.objects.create(user=self.user1, title='user1project')
        project2 = Project.objects.create(user=self.user2, title='user2project')

    def test_project_can_be_finished_by_owner_and_only_if_not_finished(self):
        """
        Ensure project can be finished and can be finished once.
        """
        tokenauth = 'Token {}'.format(self.token1)
        url = '/api/projects/1/finished/'

        response = self.client.get(url, HTTP_AUTHORIZATION=tokenauth)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotEqual(response.data['finish'], None)

        response = self.client.get(url, HTTP_AUTHORIZATION=tokenauth)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, 'project already finished')

    def test_project_can_be_finished_only_by_owner(self):
        """
        Ensure project can be finished only by owner.
        """
        tokenauth = 'Token {}'.format(self.token2)
        url = '/api/projects/1/finished/'

        response = self.client.get(url, HTTP_AUTHORIZATION=tokenauth)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_project_can_be_reopened_by_owner_and_only_if_not_open(self):
        """
        Ensure finished project can be opened by owner.
        """
        tokenauth = 'Token {}'.format(self.token1)

        # first finish project
        urlfinished = '/api/projects/1/finished/'
        self.client.get(urlfinished, HTTP_AUTHORIZATION=tokenauth)

        url = '/api/projects/1/reopen/'
        response = self.client.get(url, HTTP_AUTHORIZATION=tokenauth)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['finish'], None)

        response = self.client.get(url, HTTP_AUTHORIZATION=tokenauth)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, 'project is open')

    def test_project_can_be_reopened_only_by_owner(self):
        """
        Ensure project can be reopened only by owner.
        """
        tokenauth1 = 'Token {}'.format(self.token1)

        # first finish project
        urlfinished = '/api/projects/1/finished/'
        self.client.get(urlfinished, HTTP_AUTHORIZATION=tokenauth1)

        # user2 comes
        tokenauth2 = 'Token {}'.format(self.token2)
        url = '/api/projects/1/reopen/'
        response = self.client.get(url, HTTP_AUTHORIZATION=tokenauth2)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

class TaskCreateTests(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.user1 = CustomUser.objects.create(username="olivia", email="olivia@test.com", password="secret123")
        self.token1 = Token.objects.create(user=self.user1)

        self.user2 = CustomUser.objects.create(username="olivia2", email="olivia2@test.com", password="secret123")
        self.token2 = Token.objects.create(user=self.user2)

        project1 = Project.objects.create(user=self.user1, title='user1project')
        project2 = Project.objects.create(user=self.user2, title='user2project')
        project2b = Project.objects.create(user=self.user2, title='user2projectb')

    def test_create_task_unauthenticated(self):
        """
        Ensure that unauthenticated users cannot create a new task object.
        """
        url = '/api/tasks/'
        response = self.client.post(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(Task.objects.count(), 0)

    def test_create_task_authenticated_unauthorized(self):
        """
        Ensure that authenticated users cannot create task for not owned projects.
        """
        # self.client.force_login(user=self.user1)

        url = '/api/tasks/'
        data = {'project': 2, 'title': 'task'}
        tokenauth = 'Token {}'.format(self.token1)

        response = self.client.post(url,data, format='json', HTTP_AUTHORIZATION=tokenauth )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Task.objects.count(), 0)
        self.assertEqual(response.data, 'user does not own project')

    def test_create_task_authenticated_authorized(self):
        """
        Ensure that authenticated users can create a new task object for own project.
        """
        # self.client.force_login(user=self.user1)

        url = '/api/tasks/'
        data = {'project': 1, 'title': 'task'}
        tokenauth = 'Token {}'.format(self.token1)

        response = self.client.post(url,data, format='json', HTTP_AUTHORIZATION=tokenauth )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Task.objects.count(), 1)

    def test_create_task_title_too_long(self):
        """
        Task title max 50 chars long.
        """
        # self.client.force_login(user=self.user1)

        url = '/api/tasks/'
        data = {'project': 1, 'title': '012345678901234567890123456789012345678901234567891'}
        tokenauth = 'Token {}'.format(self.token1)

        response = self.client.post(url,data, format='json', HTTP_AUTHORIZATION=tokenauth )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_task_without_title(self):
        """
        Title field for task is required.
        """
        # self.client.force_login(user=self.user1)

        url = '/api/tasks/'
        data = {'project': 1}
        tokenauth = 'Token {}'.format(self.token1)

        response = self.client.post(url,data, format='json', HTTP_AUTHORIZATION=tokenauth )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_task_without_project(self):
        """
        Project field for task is required.
        """
        # self.client.force_login(user=self.user1)

        url = '/api/tasks/'
        data = {'title': 'test title'}
        tokenauth = 'Token {}'.format(self.token1)

        response = self.client.post(url,data, format='json', HTTP_AUTHORIZATION=tokenauth )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class TaskListRetrieveUpdateDeleteTests(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.user1 = CustomUser.objects.create(username="olivia", email="olivia@test.com", password="secret123")
        self.token1 = Token.objects.create(user=self.user1)

        self.user2 = CustomUser.objects.create(username="olivia2", email="olivia2@test.com", password="secret123")
        self.token2 = Token.objects.create(user=self.user2)

        project1 = Project.objects.create(user=self.user1, title='user1project')
        project2 = Project.objects.create(user=self.user2, title='user2project')
        project3 = Project.objects.create(user=self.user2, title='user2projectb')

        task1 = Task.objects.create(project=project1,title='task1')
        task2 = Task.objects.create(project=project1,title='task2')
        task3 = Task.objects.create(project=project2,title='task3')
        task4 = Task.objects.create(project=project3,title='task4')

    def test_tasklist_unauthenticated(self):
        """
        Ensure only authenticated user can list tasks.
        """

        url = '/api/tasks/'
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(Task.objects.count(), 4)

    def test_list_own_task1(self):
        """
        A)Ensure user can only list own task objects.
        """
        tokenauth = 'Token {}'.format(self.token1)

        url = '/api/tasks/'
        response = self.client.get(url, HTTP_AUTHORIZATION=tokenauth)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Task.objects.count(), 4)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(response.data[0]['project'], 1)
        self.assertEqual(response.data[1]['project'], 1)

    def test_list_own_task2(self):
        """
        B)Ensure user can only list own task objects.
        """
        tokenauth = 'Token {}'.format(self.token2)

        url = '/api/tasks/'
        response = self.client.get(url, HTTP_AUTHORIZATION=tokenauth)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Task.objects.count(), 4)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(response.data[0]['project'], 2)
        self.assertEqual(response.data[1]['project'], 3)

    def test_retrieve_task_unauthorized(self):
        """
        Ensure user can not get others' task object.
        """
        tokenauth = 'Token {}'.format(self.token1)

        url = '/api/tasks/3/'
        response = self.client.get(url, HTTP_AUTHORIZATION=tokenauth)

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_retrieve_task_authorized(self):
        """
        Ensure user can get owned task object.
        """
        tokenauth = 'Token {}'.format(self.token1)

        url = '/api/tasks/1/'
        response = self.client.get(url, HTTP_AUTHORIZATION=tokenauth)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_task_partialupdate_method(self):
        """
        Ensure user can not update any task
        """
        tokenauth = 'Token {}'.format(self.token1)

        url = '/api/tasks/1/'
        data = {'title': 'newtitle'}
        response = self.client.patch(url,data, HTTP_AUTHORIZATION=tokenauth)

        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_task_update_method(self):
        """
        Ensure user can not update any task
        """
        tokenauth = 'Token {}'.format(self.token1)

        url = '/api/tasks/1/'
        data = {'project': 1, 'title': 'newtitle'}
        response = self.client.put(url,data, HTTP_AUTHORIZATION=tokenauth)

        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_task_delete_method(self):
        """
        Ensure user can not delete any task.
        """
        tokenauth = 'Token {}'.format(self.token1)

        url = '/api/tasks/1/'
        response = self.client.delete(url, HTTP_AUTHORIZATION=tokenauth)

        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)


class TaskPartTests(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.user1 = CustomUser.objects.create(username="olivia", email="olivia@test.com", password="secret123")
        self.token1 = Token.objects.create(user=self.user1)

        self.user2 = CustomUser.objects.create(username="olivia2", email="olivia2@test.com", password="secret123")
        self.token2 = Token.objects.create(user=self.user2)

        project1 = Project.objects.create(user=self.user1, title='user1project')
        project2 = Project.objects.create(user=self.user2, title='user2project')

        task1 = Task.objects.create(project=project1,title='task1')
        task2 = Task.objects.create(project=project2,title='task2')

        #user1: task1, taskpart1, taskpart2
        #user2: task2, taskpart3
        taskpart1 = TaskPart.objects.create(task=task1)
        taskpart2 = TaskPart.objects.create(task=task1)
        taskpart3 = TaskPart.objects.create(task=task2)

    def test_taskpart_list_not_available(self):
        """
        ensure taskparts cannot be listed
        """
        tokenauth = 'Token {}'.format(self.token1)

        url = '/api/taskparts/'
        response = self.client.get(url, HTTP_AUTHORIZATION=tokenauth)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(TaskPart.objects.count(), 3)
        self.assertEqual(response.data, 'not available')

    def test_taskpart_retrieve_not_available(self):
        """
        ensure taskpart cannot be retrieved
        """
        tokenauth = 'Token {}'.format(self.token1)

        url = '/api/taskparts/3/'
        response = self.client.get(url, HTTP_AUTHORIZATION=tokenauth)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(TaskPart.objects.count(), 3)
        self.assertEqual(response.data, 'not available')

    def test_taskpart_create_unauthorized(self):
        """
        ensure taskpart can ONLY be created to task the user owns
        """
        tokenauth = 'Token {}'.format(self.token1)

        url = '/api/taskparts/'
        data = {'task': 2}
        response = self.client.post(url, data, HTTP_AUTHORIZATION=tokenauth)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(TaskPart.objects.count(), 3)
        self.assertEqual(response.data, 'user does not own task')

    def test_taskpart_create_authorized(self):
        """
        ensure taskpart can be created to task the user owns
        """
        tokenauth = 'Token {}'.format(self.token1)

        url = '/api/taskparts/'
        data = {'task': 1}
        response = self.client.post(url, data, HTTP_AUTHORIZATION=tokenauth)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(TaskPart.objects.count(), 4)

    def test_taskpart_create_without_task(self):
        """
        ensure taskpart cannot be created without task field
        """
        tokenauth = 'Token {}'.format(self.token1)

        url = '/api/taskparts/'
        response = self.client.post(url, HTTP_AUTHORIZATION=tokenauth)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_taskpart_can_be_stopped(self):
        """
        taskpart can be stopped once
        """
        tokenauth = 'Token {}'.format(self.token1)
        time.sleep(3) # if datetimes are too close, duration is 0!
        url = '/api/taskparts/2/stopped/'

        response = self.client.get(url, HTTP_AUTHORIZATION=tokenauth)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response2 = self.client.get(url, HTTP_AUTHORIZATION=tokenauth)
        self.assertEqual(response2.status_code, status.HTTP_200_OK)
        self.assertEqual(response2.data, 'already stopped')

    def test_taskpart_can_be_stopped_only_by_owner(self):
        """
        taskpart can be stopped only by owner
        """
        tokenauth = 'Token {}'.format(self.token1)
        time.sleep(3) # if datetimes are too close, duration is 0!
        url = '/api/taskparts/3/stopped/'

        response = self.client.get(url, HTTP_AUTHORIZATION=tokenauth)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_taskpart_delete_method(self):
        """
        Ensure user can not delete any taskpart.
        """
        tokenauth = 'Token {}'.format(self.token1)

        url = '/api/taskparts/1/'
        response = self.client.delete(url, HTTP_AUTHORIZATION=tokenauth)

        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_taskpart_update_method(self):
        """
        Ensure user can not update any taskpart
        """
        tokenauth = 'Token {}'.format(self.token1)

        url = '/api/taskparts/1/'
        data = {'task': 1}
        response = self.client.put(url,data, HTTP_AUTHORIZATION=tokenauth)

        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_taskpart_partialupdate_method(self):
        """
        Ensure user can not patch any taskpart
        """
        tokenauth = 'Token {}'.format(self.token1)

        url = '/api/taskparts/1/'
        data = {'task': 1, 'start': datetime.datetime.now()}
        response = self.client.patch(url,data, HTTP_AUTHORIZATION=tokenauth)

        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)


class ProjectDetailedTests(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.user1 = CustomUser.objects.create(username="olivia", email="olivia@test.com", password="secret123")
        self.token1 = Token.objects.create(user=self.user1)

        self.user2 = CustomUser.objects.create(username="olivia2", email="olivia2@test.com", password="secret123")
        self.token2 = Token.objects.create(user=self.user2)

        project1 = Project.objects.create(user=self.user1, title='user1project')
        project2 = Project.objects.create(user=self.user2, title='user2project')
        project2b = Project.objects.create(user=self.user2, title='user2projectb')

    def test_list_unauthenticated(self):
        """
        Ensure only authenticated user can list.
        """

        url = '/api/detailedprojects/'
        # url = reverse('project-list')
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(Project.objects.count(), 3)

    def test_list_own_projects1(self):
        """
        A)Ensure user can only list own project object.
        """
        # self.client.force_login(user=self.user1)
        tokenauth = 'Token {}'.format(self.token1)

        url = '/api/detailedprojects/'
        # url = reverse('project-list')
        response = self.client.get(url, HTTP_AUTHORIZATION=tokenauth)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Project.objects.count(), 3)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]['user'], 1)

    def test_list_own_projects2(self):
        """
        B)Ensure user can only list own project object.
        """
        # self.client.force_login(user=self.user2)
        tokenauth = 'Token {}'.format(self.token2)

        url = '/api/detailedprojects/'
        # url = reverse('project-list')
        response = self.client.get(url, HTTP_AUTHORIZATION=tokenauth)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Project.objects.count(), 3)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(response.data[0]['user'], 2)
        self.assertEqual(response.data[1]['user'], 2)

    def test_retrieve_project_authenticated_authorized(self):
        """
        Ensure user can get own project object.
        """
        # self.client.force_login(user=self.user1)
        tokenauth = 'Token {}'.format(self.token1)

        url = '/api/detailedprojects/1/'
        response = self.client.get(url, HTTP_AUTHORIZATION=tokenauth)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['user'], 1)
        self.assertEqual(response.data['id'], 1)

    def test_retrieve_project_unauthorized(self):
        """
        Ensure user can not get others' project object.
        """
        # self.client.force_login(user=self.user1)
        tokenauth = 'Token {}'.format(self.token1)

        url = '/api/detailedprojects/2/'
        response = self.client.get(url, HTTP_AUTHORIZATION=tokenauth)

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_detailedproject_delete_method(self):
        """
        Ensure user can not delete any detailed projects..
        """
        tokenauth = 'Token {}'.format(self.token1)

        url = '/api/detailedprojects/1/'
        response = self.client.delete(url, HTTP_AUTHORIZATION=tokenauth)

        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_detailedproject_update_method(self):
        """
        Ensure user can not update any taskpart
        """
        tokenauth = 'Token {}'.format(self.token1)

        url = '/api/detailedprojects/1/'
        data = {'title': 'newtitle'}
        response = self.client.put(url,data, HTTP_AUTHORIZATION=tokenauth)

        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_detailedproject_partialupdate_method(self):
        """
        Ensure user can not patch any taskpart
        """
        tokenauth = 'Token {}'.format(self.token1)

        url = '/api/detailedprojects/1/'
        data = {'title': 'newtitle'}
        response = self.client.patch(url,data, HTTP_AUTHORIZATION=tokenauth)

        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
