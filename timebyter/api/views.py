from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import status
from rest_framework import generics

import datetime

from .serializers import (
    CustomUserSerializer,
    ProjectSerializer,
    TaskSerializer,
    TaskPartSerializer,
    ProjectDetailedSerializer)

from timebyter.models import Project, Task, TaskPart
from accounts.models import CustomUser

class CustomUserView(generics.ListAPIView):
    serializer_class = CustomUserSerializer

    def get_queryset(self):
        user=self.request.user
        return CustomUser.objects.filter(id=self.request.user.id)

class ProjectViewSet(viewsets.ModelViewSet):
    serializer_class = ProjectSerializer
    http_method_names = ['get', 'post', 'delete', 'patch']

    def get_queryset(self):
        user=self.request.user
        return Project.objects.filter(user=self.request.user).order_by('-started_on')

    def perform_create(self, serializer):
        serializer.validated_data['user'] = self.request.user
        return super(ProjectViewSet, self).perform_create(serializer)

    @action(detail=True)
    def finished(self, request, **kwargs):
        project = self.get_object()
        if project.finished_on:
            return Response('project already finished')
        project.finished_on = datetime.datetime.now()
        project.save()

        serializer = ProjectSerializer(project)
        return Response(serializer.data)

    @action(detail=True)
    def reopen(self, request, **kwargs):
        project = self.get_object()
        if not project.finished_on:
            return Response('project is open')
        project.finished_on = None
        project.save()

        serializer = ProjectSerializer(project)
        return Response(serializer.data)


class TaskViewSet(viewsets.ModelViewSet):
    serializer_class = TaskSerializer
    http_method_names = ['get', 'post']

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        project = serializer.validated_data['project']
        if project.user != self.request.user:
            return Response('user does not own project')
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def get_queryset(self):
        user=self.request.user
        return Task.objects.filter(project__user=self.request.user)

class TaskPartViewSet(viewsets.ModelViewSet):
    serializer_class = TaskPartSerializer
    http_method_names = ['get','post']

    def get_queryset(self):
        user=self.request.user
        return TaskPart.objects.filter(task__project__user=self.request.user)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        task = serializer.validated_data['task']
        if task.project.user != self.request.user:
            return Response('user does not own task')
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def list(self, request, *args, **kwargs):
        return Response('not available')

    def retrieve(self, request, *args, **kwargs):
        return Response('not available')

    @action(detail=True)
    def stopped(self, request, **kwargs):
        taskpart = self.get_object()
        if taskpart.get_duration > datetime.timedelta(0):  #once was already stopped
            return Response('already stopped')
        taskpart.stop = datetime.datetime.now()
        taskpart.save()

        serializer = TaskPartSerializer(taskpart)
        return Response(serializer.data)


class ProjectDetailedViewSet(viewsets.ModelViewSet):
    serializer_class = ProjectDetailedSerializer
    http_method_names = ['get']

    def get_queryset(self):
        user=self.request.user
        return Project.objects.filter(user=self.request.user).order_by('-started_on')
