from django.urls import path, include
from .views import (
    CustomUserView, ProjectViewSet, TaskViewSet,
    TaskPartViewSet, ProjectDetailedViewSet)
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'projects', ProjectViewSet, base_name="project")
router.register(r'tasks', TaskViewSet, base_name="task")
router.register(r'taskparts', TaskPartViewSet, base_name="taskpart")
router.register(r'detailedprojects',ProjectDetailedViewSet, base_name="detailedproject")


urlpatterns = [
    path('', include(router.urls)),
    path('user/', CustomUserView.as_view()),
]
