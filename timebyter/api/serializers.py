from rest_framework import serializers

from timebyter.models import Project, Task, TaskPart
from accounts.models import CustomUser

class CustomUserSerializer(serializers.ModelSerializer):
    number_of_projects = serializers.SerializerMethodField()
    number_of_finished_projects = serializers.SerializerMethodField()
    last_login = serializers.SerializerMethodField()
    date_joined = serializers.SerializerMethodField()

    class Meta:
        model = CustomUser
        fields = ('username',  'email', 'last_login', 'date_joined',
         'number_of_projects', 'number_of_finished_projects')

    def get_number_of_projects(self, instance):
        return instance.project_set.all().count()

    def get_number_of_finished_projects(self, instance):
        return instance.project_set.all().filter(finished_on__isnull=False).count()

    def get_last_login(self, instance):
        return str(instance.last_login)[:-7]

    def get_date_joined(self, instance):
        return str(instance.date_joined)[:-7]


class ProjectSerializer(serializers.ModelSerializer):
    user = serializers.PrimaryKeyRelatedField(
        read_only=True,
    )

    class Meta:
        model = Project
        fields = ('id', 'user','title', 'description', 'start', 'finish')

class TaskPartSerializer(serializers.ModelSerializer):
    class Meta:
        model = TaskPart
        fields = ('id','task', 'duration' )


class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = ('id', 'project','title', 'description', 'duration')



class ProjectDetailedSerializer(serializers.ModelSerializer):
    tasks = serializers.SerializerMethodField()

    class Meta:
        model = Project
        fields = ('id', 'user','title', 'description',
                    'start', 'finish', 'tasks', 'number_of_tasks',
                    'project_duration', 'finished')

    def get_tasks(self, instance):
        tasks = instance.tasks.order_by('-id')
        return TaskSerializer(tasks, many=True).data
