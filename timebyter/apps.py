from django.apps import AppConfig


class TimebyterConfig(AppConfig):
    name = 'timebyter'
